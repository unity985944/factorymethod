using System;
using UnityEngine;

public enum ImperialUnitType
{
    VADER,
    TROOPER,
}

public class ImperialUnit : Unit
{
    public event Action<int, string> AfterMouseOver;
    private void OnMouseOver()
    {
        AfterMouseOver?.Invoke(hp, name);
    }
}