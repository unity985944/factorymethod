using UnityEngine;

public abstract class Factory<T>: MonoBehaviour
{
   public Unit Generate(T type, Transform parent, Vector3 position)
   {
       Unit unit = Create(type);
       unit.transform.SetParent(parent);
       unit.transform.position = position;
       return unit;
   }
   protected abstract Unit Create(T type);
}
