using System;
using UnityEngine;

public class ImperialFactory : Factory<ImperialUnitType>
{
    [SerializeField] private GameObject vader;
    [SerializeField] private GameObject trooper;

    protected override Unit Create(ImperialUnitType type)
    {
        ImperialUnit unit = type switch
        {
            ImperialUnitType.VADER =>
                Instantiate(vader).GetComponent<ImperialUnit>(),

            ImperialUnitType.TROOPER =>
                Instantiate(trooper).GetComponent<ImperialUnit>(),
        };
        unit.AfterMouseOver += UIManager.Instance.ChangeText;
        return unit;
    }
}