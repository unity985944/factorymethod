using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private Button vaderButton;

    [SerializeField] private Button trooperButton;

    [SerializeField] private Text text;

    [SerializeField] private Transform parent;

    [SerializeField] private ImperialFactory factory;

    private float x = -7.5f;

    private void Start()
    {
        vaderButton.onClick.AddListener(GenerateVader);
        trooperButton.onClick.AddListener(GenerateTrooper);
    }

    public void ChangeText(int hp, string name)
    {
        text.text = $"name: {name}\nhp: {hp}";
    }

    private void GenerateImperialUnit(ImperialUnitType type)
    {
        factory.Generate(type, parent, new Vector2(x, 3.0f));
        x += 2.5f;
    }

    private void GenerateVader()
    {
        GenerateImperialUnit(ImperialUnitType.VADER);
    }

    private void GenerateTrooper()
    {
        GenerateImperialUnit(ImperialUnitType.TROOPER);
    }
}